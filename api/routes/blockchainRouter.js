const express = require('express');
const router = express.Router();
const BlockchainInstance = require("../../blockchain/instance");

router.get("/mine", (req, res) => {
  // const minedBlock = BlockchainInstance.mineBlock('data');
  res.send({
    notes: "mined blockchain",
    blockchain: BlockchainInstance.chain
  })
});

router.post("/mine", (req, res) => {
  const { data } = req.body;
  console.log("BlockchainInstance", BlockchainInstance);
  const newBlock = BlockchainInstance.mineBlock(data);
  res.send({
    note: "block mined",
    blockchain: BlockchainInstance.chain
  })
});

module.exports = router;