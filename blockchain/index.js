const Block = require("./block");
const { DIFFICULTY } = require("./settings");

class Blockchain {

  constructor(){
    this.chain = [Blockchain.genesisBlock()];
  }

  printBlock(block){
    const { timestamp, hash , lastHash, nonce, data } = block;
    return `
      BLOCK         
        timestamp   : ${timestamp}
        hash        : ${hash}
        lastHash    : ${lastHash}
        nonce       : ${nonce}  
        data        : ${data}    
    `
  }

  static genesisBlock(){
    return new Block('723621600000','Genesis Hash', '--', 4, 'Genesis Block', 0);
  }

  getLastBlock(){
    return this.chain[this.chain.length - 1];
  }

  mineBlock(data) {

    const lastHash = this.getLastBlock().hash;
    let nonce = 0;
    let hash;
    let timestamp;

    do {
      nonce++;
      timestamp = Date.now();
      hash = Block.generateHash(timestamp, lastHash, nonce, data);
    } while (hash.substring(0,DIFFICULTY) !== ("0").repeat(DIFFICULTY));

    let id = this.chain.length

    return this.addBlock(timestamp, hash , lastHash, nonce, data, id)

  }

  addBlock(timestamp, hash , lastHash, nonce, data, id){
    const newBlock = new Block(timestamp, hash, lastHash, nonce, data, id);
    this.chain.push(newBlock);
    return newBlock;
  }

  isValidChain(chain){
    // loop through chain.....
    for(let i = 1; i < this.chain.length; i++) {
      const previousBlock = this.chain[i - 1];
      const currentBlock = this.chain[i];
      const currentRehash =  Block.generateHash(currentBlock.timestamp, currentBlock.lastHash, currentBlock.nonce, currentBlock.data);
      // ....make sure hash/last hash matches (for all blocks)
      if(previousBlock.hash !== currentBlock.lastHash) {
        return false;
      }
      // ....make sure data hasn't been tampered with (for all blocks)
      if(currentRehash !== currentBlock.hash) {
        return false;
      }
    }
    // ....only accept longer chain ~> best chance of being valid
    return !(chain.length < this.chain.length);
  }

}

module.exports = Blockchain;