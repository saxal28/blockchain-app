const sha256 = require("sha256");

class Block {
  constructor(timestamp, hash, lastHash, nonce, data, id){
    this.timestamp = timestamp;
    this.lastHash = lastHash;
    this.nonce = nonce;
    this.hash = hash;
    this.data = data;
    this.id = id;
  }

  static generateHash(timestamp, lastHash, nonce, data){
    return sha256(`${timestamp}${lastHash}${data}${nonce}`);
  }

}

module.exports = Block;