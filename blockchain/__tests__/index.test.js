const Blockchain = require("../index");
const Block = require("../block");
let bitcoin;

describe("Blockchain....", () => {
  beforeEach(() => {
    bitcoin = new Blockchain();
  });

  it("should have a genesis block", () => {
    expect(bitcoin.chain.length).toEqual(1);
  });

  it("should have a valid genesis block", () => {
    const firstBlock = JSON.stringify(bitcoin.chain[0]);
    const genesisBlock = JSON.stringify(Blockchain.genesisBlock());

    expect(firstBlock).toBe(genesisBlock);
  });

  it("should add a valid block to the blockchain", () => {
      const newBlock = bitcoin.mineBlock("data!");
      const lastBlock = bitcoin.chain[0];

      expect(bitcoin.chain.length).toEqual(2);
      expect(newBlock.lastHash).toEqual(lastBlock.hash);

      expect(bitcoin.isValidChain(bitcoin.chain)).toBe(true);

      // let's try to modify data
      // the test should fail
      bitcoin.chain[1].data = "i tried to modify the data";
      expect(bitcoin.isValidChain(bitcoin.chain)).toBe(false);

  });

});