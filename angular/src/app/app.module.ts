import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { BlockchainComponent } from './components/blockchain/blockchain.component';
import { ChatTestComponent } from './components/chat-test/chat-test.component';
import {FormsModule} from "@angular/forms";
import {RouterModule, Routes} from "@angular/router";


const routes: Routes = [
  { path: '', component: BlockchainComponent },
  { path: 'chat-test', component: ChatTestComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    BlockchainComponent,
    ChatTestComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routes),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
