import { Component, OnInit } from '@angular/core';
import {BlockchainService} from "../../services/blockchain.service";
import toastr from "toastr";

@Component({
  selector: 'app-blockchain',
  templateUrl: './blockchain.component.html',
  styleUrls: ['./blockchain.component.css']
})
export class BlockchainComponent implements OnInit {
  status = [];
  blocks: any;
  isMining = false;
  blockData: string;
  activeBlock: any;

  constructor(private blockchainService: BlockchainService) {
    this.blockchainService.syncChains()
      .subscribe(data => {
        if(JSON.stringify(this.blockchainService.localBlockchain) !== JSON.stringify(data)) {
          this.blockchainService.localBlockchain = data;
          this.blocks = data;
          this.activeBlock = data[0];
          this.status.push("chain up to date.");
        }
      });
    this.blockchainService.blockMined()
      .subscribe(data => {
        this.blocks = data;
        this.isMining = false;
        this.blockData = "";

        this.sendToast();
      });
  }

  ngOnInit() {
    this.blockchainService.userConnected();
    if(this.blockchainService.localBlockchain) {
      this.blocks = this.blockchainService.localBlockchain;
      this.activeBlock = this.blockchainService.localBlockchain[0];
    }
  }

  handleMineBlock(){
    console.log("MINED BLOCK PRESSED", this.blockData);
    this.isMining = true;
    this.blockchainService.mineBlock(this.blockData);
  }

  setActiveBlock(block: any) {
    this.activeBlock = block;
  }

  sendToast(){
    toastr.options = {
      "closeButton": false,
      "debug": false,
      "newestOnTop": true,
      "progressBar": true,
      "positionClass": "toast-bottom-right",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }

    toastr.success('Block Mined!');

  }

}
