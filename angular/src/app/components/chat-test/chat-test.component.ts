import { Component, OnInit } from '@angular/core';
import {BlockchainService} from "../../services/blockchain.service";
import { isDevMode } from '@angular/core';

@Component({
  selector: 'app-chat-test',
  templateUrl: './chat-test.component.html',
  styleUrls: ['./chat-test.component.css']
})
export class ChatTestComponent implements OnInit {
  message: string;
  messages = [];
  isDevMode: boolean;

  constructor(private blockchainService: BlockchainService) {
    this.blockchainService.messageReceived()
      .subscribe(data => {
        console.log("DATA FROM OBSERVABLE", data);
        this.messages.push(data);
      })

  }

  ngOnInit() {

    // this.blockchainService.userConnected();
    this.isDevMode = isDevMode();
  }

  updateMessage(e) {
    this.message = e.target.value;
    console.log('message', this.message);
  }

  sendMessage(){
    this.blockchainService.sendMessage(this.message);
    this.message = '';
  }

}
