import { Injectable } from '@angular/core';
import * as io from "socket.io-client";
import {observable, Observable} from "rxjs/index";
import { isDevMode } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class BlockchainService {
  message: string;
  socket: any;
  localBlockchain: any;

  constructor() {
    console.log('Blockchain Service Connected!');
    this.socket = isDevMode() ? io('http://localhost:3000') : io();
  }

  //actions -> socket emitters
  userConnected(){
    if(!this.localBlockchain) {
      console.log("user connected");
      this.socket.emit("sync-chain");
    }
  }

  mineBlock(blockdata: any){
    this.socket.emit("mine block", blockdata);
  }

  sendMessage(data){
    this.socket.emit("message", data);
  }

  //socket listeners
  syncChains(){
    return new Observable(observer => {
      this.socket.on("synced chain", data => {
        observer.next(data);
      })
    })
  }

  blockMined(){
    return new Observable(observer => {
      this.socket.on('block mined', data => {
        observer.next(data)
      })
    })
  }

  messageReceived(){
    return new Observable(observer => {
      this.socket.on("message received", data => {
        observer.next(data);
      })
    });
  }

}
