var createError = require('http-errors');
var express = require('express');
var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');

var app = express();
var http = require('http');
var debug = require('debug')('myapp:server');

//routes
var blockchainRouter = require("./api/routes/blockchainRouter");
const BlockchainInstance = require("./blockchain/instance");

var port = normalizePort(process.env.PORT || '3000');
app.set('port', port);

app.use(logger('dev'));
app.use(express.json());
app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'angular/dist/app-main')));

// view engine setup
app.set('view engine', 'ejs');
app.use('/', blockchainRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


// TODO -- create separate class for Chat socket
var server = http.createServer(app);
var io = require('socket.io').listen(server);

// TODO -- socket.io stuff
io.on('connection', function(socket){
  console.log('a user connected');

  socket.on('message', (data) => {
    io.emit("message received", data);
  });

});

//TODO create separate class for Blockchain socket
io.on('connection', function(socket){

  console.log('Blockchain Peer Connected');

  socket.on('new user', (data) => {
    io.emit("peer joined", 'peer has connected');
    io.emit("synced chain", BlockchainInstance.chain);
  });

  socket.on('sync-chain', () => {
    io.emit("synced chain", BlockchainInstance.chain);
  });

  socket.on("mine block", (data) => {
    console.log("MINING BLOCK");
    //mine block with validation
    BlockchainInstance.mineBlock(data || "no data specified");
    //consensus algorithm
    io.emit("block mined", BlockchainInstance.chain)
  });

});

server.listen(port, () => console.log("listening on ", port));
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {

  console.log("ERROR", error);

  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}
